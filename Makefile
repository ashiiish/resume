.PHONY: build build-victoria build-docker build-docker-victoria

IMAGE=blang/latex:ubuntu
PWD=$(shell pwd)
UID=$(shell id -u)
GID=$(shell id -g)

build:
	pdflatex resume.tex

build-victoria:
	pdflatex victoria.tex

build-docker:
	exec docker run --rm -i --user="${UID}:${GID}" --net=none -v "${PWD}":/data "${IMAGE}" pdflatex resume.tex

build-docker-victoria:
	exec docker run --rm -i --user="${UID}:${GID}" --net=none -v "${PWD}":/data "${IMAGE}" pdflatex victoria.tex

build-podman:
	exec podman run --rm -i --net=none -v "${PWD}":/data "${IMAGE}" pdflatex resume.tex

build-podman-victoria:
	exec podman run --rm -i --net=none -v "${PWD}":/data "${IMAGE}" pdflatex victoria.tex
